# Kenc.ACMELib - Open Source
![Build status](https://kenc.visualstudio.com/ACME/_apis/build/status/Build)
[![Feature Requests](https://img.shields.io/github/issues/Kencdk/Kenc.ACMELib/feature-request.svg)](https://github.com/Kencdk/Kenc.ACMELib/issues?q=is%3Aopen+is%3Aissue+label%3Afeature-request+sort%3Areactions-%2B1-desc)
[![Bugs](https://img.shields.io/github/issues/Kencdk/Kenc.ACMELib/bug.svg)](https://github.com/Kencdk/Kenc.ACMELib/issues?utf8=✓&q=is%3Aissue+is%3Aopen+label%3Abug)

C# Implementation of the [ACME protocol v2](https://tools.ietf.org/html/draft-ietf-acme-acme-11)

## Contributing

## Getting Started

## Feedback

* Request a new feature using [Github Issues](https://github.com/Kencdk/Kenc.ACMELib/issues).
* File a bug in [GitHub Issues](https://github.com/Kencdk/Kenc.ACMELib/issues).
* [Tweet](https://twitter.com/kenmandk) with any other feedback.

## Related Projects


## License

Copyright (c) Ken Christensen. All rights reserved.

Licensed under the [MIT](LICENSE) License.